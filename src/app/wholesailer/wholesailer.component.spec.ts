import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WholesailerComponent } from './wholesailer.component';

describe('WholesailerComponent', () => {
  let component: WholesailerComponent;
  let fixture: ComponentFixture<WholesailerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WholesailerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WholesailerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
