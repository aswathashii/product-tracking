import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { ManufactureComponent } from './manufacture/manufacture.component';
import { CustomsDepComponent } from './customs-dep/customs-dep.component';
import { RetailerComponent } from './retailer/retailer.component';
import { WholesailerComponent } from './wholesailer/wholesailer.component';
import { CustomerComponent } from './customer/customer.component';
import { BsNavebarComponent } from './bs-navebar/bs-navebar.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ManufactureComponent,
    CustomsDepComponent,
    RetailerComponent,
    WholesailerComponent,
    CustomerComponent,
    BsNavebarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot([
      {path:'',component:HomeComponent},
      {path:'manufacture',component:ManufactureComponent},
      {path:'customer',component:CustomerComponent},
      {path:'reatailer',component:RetailerComponent},
      {path:'wholesailer',component:WholesailerComponent},
      {path:'customs',component:CustomsDepComponent},


    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
