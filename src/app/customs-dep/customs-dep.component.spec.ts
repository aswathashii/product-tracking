import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomsDepComponent } from './customs-dep.component';

describe('CustomsDepComponent', () => {
  let component: CustomsDepComponent;
  let fixture: ComponentFixture<CustomsDepComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomsDepComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomsDepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
