import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BsNavebarComponent } from './bs-navebar.component';

describe('BsNavebarComponent', () => {
  let component: BsNavebarComponent;
  let fixture: ComponentFixture<BsNavebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BsNavebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BsNavebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
